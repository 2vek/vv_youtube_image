<?php

class VV_Youtube_Image extends Plugin {
  private $host;

  function about() {
    return array(2.1,
      "Embed Video Thumbnail in Youtube RSS feeds",
      "2vek");
  }

  function init($host) {
    $this->host = $host;
    $host->add_hook($host::HOOK_RENDER_ENCLOSURE, $this);
  }

  private function youtube_thumbnail_embed($video_id) {
      $img_proxy = '/public.php?op=pluginhandler&plugin=af_zz_imgproxy&pmethod=imgproxy&url=';
      if(!PluginHost::getInstance()->get_plugin('af_zz_imgproxy')) { $img_proxy = ''; }
      $yt_href = "https://www.youtube.com/watch?v=${video_id}";
      $yt_thumb = "${img_proxy}https://i.ytimg.com/vi/${video_id}/0.jpg";
      $yt_w = "280"; $yt_h = "210";
      return "<a href='${yt_href}' target='_blank'>
        <img width='${yt_w}' height='${yt_h}' src='${yt_thumb}'></img></a>";
  }

  function hook_render_enclosure($entry, $hide_images) {
    $matches = array();
    $youtube_patt = array(
      "/\/\/www\.youtube\.com\/v\/([\w-]+)/",
      "/\/\/www\.youtube\.com\/watch?v=([\w-]+)/",
      "/\/\/youtu.be\/([\w-]+)/",
    );

    if (preg_match($youtube_patt[0], $entry["url"], $matches) ||
      preg_match($youtube_patt[1], $entry["url"], $matches) ||
      preg_match($youtube_patt[2], $entry["url"], $matches)) {
      return $this->youtube_thumbnail_embed($matches[1]);
    }
  }

  // TODO: replace youtube embed with thumbnails

  function api_version() {
    return 2;
  }
}

